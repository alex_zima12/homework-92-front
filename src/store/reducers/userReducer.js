import {
    LOGIN_USERS_FAILURE,
    LOGIN_USERS_SUCCESS,
    LOGOUT_USER,
    REGISTER_USERS_FAILURE
} from "../actionTypes";

const initialState = {
    registerError: null,
    loginError: null,
    user: null
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case REGISTER_USERS_FAILURE:
            return {...state, registerError: action.error};
        case LOGIN_USERS_SUCCESS:
            return {...state, user: action.user, loginError: null};
        case LOGIN_USERS_FAILURE:
            return {...state, loginError: action.error};
        case LOGOUT_USER:
            return {...state, user: null}
        default:
            return state;
    }
};

export default usersReducer;