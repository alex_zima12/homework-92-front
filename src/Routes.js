import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Chat from "./containers/Chat/Chat";
import Main from "./components/Main/Main";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
    return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />
};

const Routes = ({user}) => {
    return (
        <Switch>
            <Route
                path='/'
                exact
                component={Main}
            />
            <ProtectedRoute
                path='/chat'
                exact
                component={Chat}
                isAllowed={user}
                redirectTo="/login"
            />
            <ProtectedRoute
                path='/register'
                exact
                component={Register}
                isAllowed={!user}
                redirectTo="/"
            />
            <ProtectedRoute
                path='/login'
                exact
                component={Login}
                isAllowed={!user}
                redirectTo="/"
            />
        </Switch>
    );
};

export default Routes;