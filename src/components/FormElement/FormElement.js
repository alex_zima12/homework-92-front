import React from 'react';
import PropTypes from "prop-types";

const FormElement = (props) => {
    return (
        <div className="form-group my-4" id={props.name} onChange={props.onChange}>
            <label htmlFor="formElement">{props.label}</label>
            <input type={props.type}
                   className="form-control"
                   id={props.name}
                   name={props.name}
                   // required
            />
            <small id="emailHelp" className="form-text text-danger">{props.helperText}</small>
        </div>
    );
};

FormElement.propTypes = {
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    type: PropTypes.string,
    error: PropTypes.string
}

export default FormElement;