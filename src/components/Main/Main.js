import React from 'react';
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";

const Main = () => {

    const user = useSelector(state => state.users.user);

    return (
        <div className="container">
           { !user ?
            <NavLink
                to="/login"
                activeStyle={{
                    fontWeight: "bold",
                    color: "red"
                }}
            >
                В чат смогут входить только авторизованные пользователи
            </NavLink> :
            <NavLink
                to="/chat"
                activeStyle={{
                    fontWeight: "bold",
                    color: "red"
                }}
            >
                Перейти в чат
            </NavLink>
        }
        </div>
    );
};

export default Main;