import React from 'react';
import {logoutUser} from "../../store/actions/userActions";
import {useDispatch, useSelector} from "react-redux";
import {apiURL} from "../../constants";

const mystyle = {
    width: "50px",
    height:"50px"
};

const UserMenu = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    console.log(user.user.avatarImage,"user");
    const logout = () => {
        dispatch(logoutUser());
    };

    let cardImage;
    if (user.user.avatarImage) {
        cardImage =  apiURL + "/uploads/" + user.user.avatarImage;
    }

    return (
        <>
            <div className="ml-auto">
                Hello, {user.user.username}
                <img src={cardImage}
                     className="rounded circle rounded-circle m-3"
                     style={mystyle}
                     alt="user"
                />
            </div>
            <button type="button" className="btn btn-outline-warning ml-3" onClick={logout}>Logout</button>
        </>
    );
};

export default UserMenu;