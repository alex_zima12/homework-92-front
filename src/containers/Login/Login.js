import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/FormElement/FormElement";
import {loginUser} from "../../store/actions/userActions";
import FacebookLogin from "../../components/FacebookLogin/FacebookLogin";

const Login = () => {
    const [state, setState] = useState({
        username: "",
        password: ""
    });

    const error = useSelector(state => state.users.loginError);
    const dispatch = useDispatch();

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(loginUser({...state}));
    };

    return (
        <div>
            <h1>Sign In</h1>
            {error && <div className="alert alert-danger mt-5" role="alert">
                {error.error}
            </div>}
            <form className="mb-5" onSubmit={formSubmitHandler} >
                <FormElement
                    name='username'
                    label='Enter username or e-mail'
                    // type='text'
                    value={state.username}
                    // helperText={getFieldError('username')}
                    onChange={inputChangeHandler}
                />
                <FormElement
                    name='password'
                    label="Password"
                    type='password'
                    value={state.password}
                    // helperText={getFieldError('password')}
                    onChange={inputChangeHandler}
                />
                <FacebookLogin/>
                <button type="submit" className="btn btn-primary">Sign Up</button>
            </form>
        </div>
    );
};

export default Login;