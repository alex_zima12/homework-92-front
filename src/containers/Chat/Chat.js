import React, {useState, useEffect, useRef} from "react";
import {useSelector} from "react-redux";
import ReconnectingWebSocket from 'reconnecting-websocket';
import Moment from 'react-moment';

const Chat = () => {
    const user = useSelector(state => state.users.user);
    const [messages, setMessages] = useState([]);
    const [activeConUsers, setActiveConUsers] = useState([]);
    const [messageText, setMessageText] = useState("");

    const ws = useRef(null);

    useEffect(() => {

        let connection = new ReconnectingWebSocket("ws://localhost:8000/chat?token=" + user.token)
        connection.debug = true;
        connection.currenttimeoutInterval = 5400;

        ws.current = connection;

        ws.current.onopen = async () => {
            await ws.current.send(JSON.stringify({type: "GET_ALL_MESSAGES"}));
            await ws.current.send(JSON.stringify({type: "GET_ALL_USERS"}));
            console.log('frontend onopen');
        };

        ws.current.onclose = () => console.log("ws connection closed");

        ws.current.onmessage = e => {
            console.log('frontend message');
            const decodedMessage = JSON.parse(e.data);
            if (decodedMessage.type === "NEW_MESSAGE") {
                setMessages(messages => [...messages, decodedMessage.message]);
            } else if (decodedMessage.type === "ALL_MESSAGES") {
                setMessages(messages => [...messages, ...decodedMessage.messages]);
            }
            else if (decodedMessage.type === "ALL_USERS") {
                const users = decodedMessage.activeUsers /// {} -> []
                const res = Object.keys(users).map(key => {
                    return users[key];
                })

                setActiveConUsers( [...activeConUsers, ...res]);
            }
        };

        return () => ws.current.close();
    }, [user.token]);


    const changeMessage = e => {
        setMessageText(e.target.value);
    };

    const sendMessage = e => {
        e.preventDefault();
        ws.current.send(JSON.stringify({
            type: "CREATE_MESSAGE",
            text: messageText,
            userName: user.username
        }));
    };

    let chat = (
        <>

        <div>
            {
                messages.map((message, idx) => {
                    return <div key={idx}>
                        <div>
                            <Moment format="YYYY/MM/DD">{message.datetime}</Moment>
                        </div>
                        <b>{message.username}: </b>
                        {message.text}
                    </div>
                })
            }

            <form onSubmit={sendMessage}>
                <input
                    type="text"
                    value={messageText}
                    onChange={changeMessage}
                    required
                />
                <button>Send</button>
            </form>

            <div>
                <p>Now online</p>
                {
                    activeConUsers.map((au, idx) => {
                        return (<div key={idx}>{au}</div>)
                    })
                }
            </div>

        </div>
            </>
    );


    return (
        <div className="App">
            {chat}
        </div>
    );
};

export default Chat;