import React from 'react';
import {useSelector} from "react-redux";
import AnonymousMenu from "./components/Menu/AnonymousMenu";
import UserMenu from "./components/Menu/UserMenu";
import Routes from "./Routes";

const App = () => {
  const user = useSelector(state => state.users.user);

  return (
      <>
        <div className="container">
          <nav className="nav">
            {!user ?
                <AnonymousMenu/>
                :
                <UserMenu/>
            }
          </nav>
          <Routes user={user} />
        </div>
      </>
  );
};

export default App;